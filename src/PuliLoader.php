<?php

namespace Acme\Translator;

use Puli\Discovery\Api\Discovery;
use Symfony\Component\Yaml\Yaml;

class PuliLoader implements TranslationLoader
{
    private $discovery;

    public function __construct(Discovery $discovery)
    {
        $this->discovery = $discovery;
    }

    public function loadTranslations($language, $domain)
    {
        $bindings = $this->discovery->findBindings('acme/translations');
        $messages = array();
        $suffix = '.'.$language.'.yml';

        foreach ($bindings as $binding) {
            foreach ($binding->getResources() as $resource) {
                if ($suffix === substr($resource->getName(), -strlen($suffix))) {
                    $messages = array_replace(
                        $messages,
                        Yaml::parse($resource->getBody())
                    );
                }
            }
        }

        return $messages;
    }
}
