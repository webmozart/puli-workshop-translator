<?php

namespace Acme\Translator;

interface TranslationLoader
{
    public function loadTranslations($language, $domain);
}
