<?php

namespace Acme\Translator\Twig;

use Acme\Translator\Translator;
use Twig_Extension;
use Twig_SimpleFunction;

class TranslatorExtension extends Twig_Extension
{
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function getName()
    {
        return 'acme_translator';
    }

    public function getFunctions()
    {
        return array(
            new Twig_SimpleFunction('trans', array($this, 'trans')),
        );
    }

    public function trans($message, $domain = 'messages')
    {
        return $this->translator->translate($message, $domain);
    }

}
