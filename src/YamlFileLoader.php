<?php

namespace Acme\Translator;

use Symfony\Component\Yaml\Yaml;

class YamlFileLoader implements TranslationLoader
{
    private $baseDir;

    public function __construct($baseDir)
    {
        $this->baseDir = $baseDir;
    }

    public function loadTranslations($language, $domain)
    {
        return Yaml::parse(file_get_contents($this->baseDir.'/'.$domain.'.'.$language.'.yml'));
    }
}
