<?php

namespace Acme\Translator;

class Translator
{
    /**
     * @var TranslationLoader
     */
    private $loader;

    private $language = 'en';

    private $catalogs = array();

    public function __construct(TranslationLoader $loader)
    {
        $this->loader = $loader;
    }

    public function setLanguage($language)
    {
        $this->language = $language;
    }

    public function translate($message, $domain = 'messages')
    {
        if (!isset($this->catalogs[$this->language][$domain])) {
            $this->catalogs[$this->language][$domain] = $this->loader->loadTranslations($this->language, $domain);
        }

        if (!isset($this->catalogs[$this->language][$domain][$message])) {
            return $message;
        }

        return $this->catalogs[$this->language][$domain][$message];
    }

}
